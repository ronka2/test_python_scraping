def exercise_one():
    for i in range(1,100):
        if (i % 3 == 0 and i % 5 == 0):
            print("ThreeFive")
        elif (i % 3 == 0):
            print("Three")
        elif (i % 5 == 0):
            print("Five")
        else:
            print(str(i));


def find_missing_nb(arr):
    n = len(arr) 
    total = (n + 1)*(n + 2)/2
    #on prend la somme de la liste en supposant qu'il n'y a pas de valeur manquant
    sum_of_arr = sum(arr) 
    #on retourne la différence entre la valeur supposé et la valeur réelle
    return total - sum_of_arr 

def calculate(arr):
    ans = 0
    #on test notre input pour valider la forme
    if isinstance(arr,list):
        for i in arr:
            #on test si l element est un string
            if isinstance(i,str):
                try:
                    #on essaie de faire la conversion
                    ans = ans + int(i)
                except Exception:
                    #si ca marche pas on continue
                    continue
    else:
        ans = False
                    
    return ans

def sortNonNegative(arr):
    n = len(arr)
    ans=[] 
    for i in range(n): 
        if (arr[i] >= 0): 
            ans.append(arr[i]) 
  
    #mettre en ordre les valeurs non négatives
    ans = sorted(ans) 
  
    j = 0
    for i in range(n): 
  
        # changer les valeurs positives dans la liste originale
        # par les valeurs mis en ordre
        if (arr[i] >= 0): 
            arr[i] = ans[j] 
            j += 1
  
    # afficher la liste 
    for i in range(n): 
        print(arr[i],end = " ") 


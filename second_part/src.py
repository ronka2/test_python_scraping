from sys import maxsize

class MySet(set):
    def __init__(self, arr=[]):
        self.myset = set(arr)
        
    def __add__(self,thatset):
        return self.myset.union(thatset.myset)
    
    def __sub__(self,thatset):
        return self.myset.difference(thatset.myset)


def decorator_check_max_int(func):
    def inner(a,b):
        rep = func(a,b)
        if ( (a + b) > maxsize):
            rep = maxsize
        return rep
        
    return inner


@decorator_check_max_int
def add(a, b):
    return a + b


import pytest
def ignore_exception(exception):
    def inner(*args, **kwargs):
        try:
            return exception(*args, **kwargs)
        except Exception as e:
            return None
    return inner

@ignore_exception
def div(a, b):
    return a / b


@ignore_exception
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap

def mult(a,b):
    return a*b


class MetaInherList(type):
    # todo exercise 5
    pass


class Ex(list):
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass

